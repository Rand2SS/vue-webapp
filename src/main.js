import Vue from 'vue';
import vueBar from 'vuebar';
import App from './App.vue';
import router from './router';
import store from './store/index';
import { CHECK_SUCCESS } from './store/actions/auth';
import axios from './axios/axios';
import moment from 'moment';

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$moment = moment;

Vue.use(vueBar);


// set token after refresh
const token = localStorage.getItem('user-token');
if (token) {
  store.commit(CHECK_SUCCESS, token);
}

// polyfill
(function (ELEMENT) {
  ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector ||
    ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
  ELEMENT.closest = ELEMENT.closest || function closest(selector) {
    if (!this) return null;
    if (this.matches(selector)) return this;
    if (!this.parentElement) { return null; }
    return this.parentElement.closest(selector);
  };
}(Element.prototype));

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window;
    for (let i = 0; i < this.length; i += 1) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}

new Vue({
  router,
  axios,
  store,
  render: h => h(App),
}).$mount('#app');
