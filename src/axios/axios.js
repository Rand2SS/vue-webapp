import axios from 'axios';

const API_URL = process.env.NODE_ENV === 'production' ? process.env.VUE_APP_API_PROD_URL : process.env.VUE_APP_API_DEV_URL;

export default axios.create({
  baseURL: API_URL,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
  },
  auth: {
    username: 'wi1',
    password: '123456',
  },
});

export const axiosDefault = axios.create({
  baseURL: API_URL,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
  },
  auth: {
    username: 'wi1',
    password: '123456',
  },
});

export const bigstoRest = axios.create({
  baseURL: 'https://bigsto.ru/rest',
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
  },
});
