export const toggleDropdown = (elem) => {
  if (!elem.classList.contains('form__select-dropdown--hidden')) {
    elem.classList.add('form__select-dropdown--hidden');
  } else {
    elem.classList.remove('form__select-dropdown--hidden');
  }
};

export const itemSelect = (index, event) => {
  const itemParent = event.target.parentNode.parentNode;
  const itemParentSelectNode = itemParent.querySelector('.form__select');
  const currentItem = itemParent.querySelector('.form__select-current');
  itemParentSelectNode.value = event.target.innerHTML;
  currentItem.innerHTML = event.target.innerHTML;
};

export const formatMoney = (value) => {
  if (value) {
    return value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
  } return 0;
};
