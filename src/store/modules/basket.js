/* eslint no-shadow: ["error", { "allow": ["state"] }] */

import axios from '@/axios/axios';
import { GET_BASKET_SMALL } from '../actions/basket';

const state = {
  count: '',
  sum: '',
};

const getters = {
  getBasketItemsCount: state => state.count,
  getBasketItemsQuantity: state => state.sum,
};

const actions = {
  [GET_BASKET_SMALL]: ({ commit }) => {
    axios.post('/cart/get', { short: true })
      .then((resp) => {
        commit(GET_BASKET_SMALL, resp.data);
      })
      .catch(err => err);
  },
};

const mutations = {
  [GET_BASKET_SMALL]: (state, data) => {
    state.count = data.count;
    state.sum = data.sum;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
