/* eslint no-shadow: ["error", { "allow": ["state"] }] */
import Vue from 'vue';
import {
  SET_USER_ID,
  SET_USER_ROLE,
  SELECT_ADDRESS,
  SELECT_CONTRACTOR,
  SELECT_PICK_UP_POINT,
  TYPE_IS_DELIVERY,
  CLEAR_ADDRESS,
  CLEAR_PICK_UP_POINT,
  CLEAR_ALL,
  CHECK_OUT_AVAILABLE, SET_USER, GET_USER,
} from '@/store/actions/user';
import userApi from '@/api/user';

const state = {
  userId: localStorage.getItem('userId') || '',
  userRole: localStorage.getItem('userRole') || '',
  selectedAddress: {
    guid: localStorage.getItem('addressGuid') || '',
    name: localStorage.getItem('addressName') || '',
  },
  userSelectedContractor: {
    guid: localStorage.getItem('contractorGuid') || '',
    name: localStorage.getItem('contractorName') || '',
  },
  selectedPickUpPoint: {
    guid: localStorage.getItem('pickUpPointGuid') || '',
    name: localStorage.getItem('pickUpPointName') || '',
  },
  typeIsDelivery: localStorage.getItem('typeIsDelivery') === 'true',
  checkOutAvailable: localStorage.getItem('checkOutAvailable') || '',
};

const getters = {
  getUserId: state => state.userId,
  getUserRole: state => state.userRole,
  getSelectedContractor: state => state.userSelectedContractor,
  getSelectedAddress: state => state.selectedAddress,
  getSelectedPickUpPoint: state => state.selectedPickUpPoint,
  getTypeIsDelivery: state => state.typeIsDelivery === 'true' || state.typeIsDelivery,
};

const actions = {
  [GET_USER]: async ({ commit }) => {
    const resp = await userApi.getUser();
    const user = resp.data;
    commit(SET_USER, user);
  },
  [SELECT_ADDRESS]: ({ commit }, { guid, name }) => {
    commit(SELECT_ADDRESS, { guid, name });
    localStorage.setItem('addressGuid', guid);
    localStorage.setItem('addressName', name);
  },
  [SELECT_CONTRACTOR]: ({ commit }, { guid, name }) => {
    commit(SELECT_CONTRACTOR, { guid, name });
    localStorage.setItem('contractorGuid', guid);
    localStorage.setItem('contractorName', name);
  },
  [SET_USER_ID]: ({ commit }, userId) => {
    commit(SET_USER_ID, userId);
    localStorage.setItem('userId', userId);
  },
  [SET_USER_ROLE]: ({ commit }, userRole) => {
    commit(SET_USER_ROLE, userRole);
    localStorage.setItem('userRole', userRole);
  },
  [SELECT_PICK_UP_POINT]: ({ commit }, { guid, name }) => {
    commit(SELECT_PICK_UP_POINT, { guid, name });
    localStorage.setItem('pickUpPointGuid', guid);
    localStorage.setItem('pickUpPointName', name);
  },
  [TYPE_IS_DELIVERY]: ({ commit }, typeState) => {
    commit(TYPE_IS_DELIVERY, typeState);
    localStorage.setItem('typeIsDelivery', typeState);
  },
  [CHECK_OUT_AVAILABLE]: ({ commit }, checkOutAvailability) => {
    commit(CHECK_OUT_AVAILABLE, checkOutAvailability);
  },
};

const mutations = {
  [SET_USER]: (state, user) => {
    Object.keys(user).forEach(key => Vue.set(state, key, user[key]));
  },
  [SELECT_ADDRESS]: (state, address) => {
    state.selectedAddress = address;
    state.typeIsDelivery = true;
  },
  [SELECT_CONTRACTOR]: (state, contractor) => {
    state.userSelectedContractor = contractor;
  },
  [SELECT_PICK_UP_POINT]: (state, pickUpPoint) => {
    state.selectedPickUpPoint = pickUpPoint;
    state.typeIsDelivery = false;
  },
  [SET_USER_ID]: (state, userId) => {
    state.userId = userId;
  },
  [SET_USER_ROLE]: (state, userRole) => {
    state.userRole = userRole;
  },
  [CLEAR_ALL]: (state) => {
    Object.keys(state.selectedAddress).forEach((key) => {
      state.selectedAddress[key] = '';
    });
    Object.keys(state.userSelectedContractor).forEach((key) => {
      state.userSelectedContractor[key] = '';
    });
    Object.keys(state.selectedPickUpPoint).forEach((key) => {
      state.selectedPickUpPoint[key] = '';
    });
    state.typeIsDelivery = false;
  },
  [CLEAR_ADDRESS]: (state) => {
    Object.keys(state.selectedAddress).forEach((key) => {
      state.selectedAddress[key] = '';
    });
  },
  [CLEAR_PICK_UP_POINT]: (state) => {
    Object.keys(state.selectedPickUpPoint).forEach((key) => {
      state.selectedPickUpPoint[key] = '';
    });
  },
  [TYPE_IS_DELIVERY]: (state, typeState) => {
    state.typeIsDelivery = typeState;
  },
  [CHECK_OUT_AVAILABLE]: (state, checkOutAvailability) => {
    localStorage.setItem('checkOutAvailable', checkOutAvailability);
    state.checkOutAvailable = checkOutAvailability;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
