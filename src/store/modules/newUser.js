/* eslint no-shadow: ["error", { "allow": ["state"] }] */
import Cookies from 'js-cookie';
import { NEW_GET_USER, NEW_SELECT_ADDRESS, NEW_SET_USER } from '@/store/actions/newUser.js';
import userApi from '@/api/new/user.js';

const state = {
  currentAddress: Cookies.get('selectedAddress') ? JSON.parse(Cookies.get('selectedAddress')) : {},
  user: null,
};

const getters = {};

const actions = {
  [NEW_GET_USER]: async ({ commit }) => {
    const resp = await userApi.getUser();
    commit(NEW_SET_USER, resp.data);
  },
};

const mutations = {
  [NEW_SELECT_ADDRESS]: (state, address) => {
    state.currentAddress = address;
    Cookies.set('selectedAddress', address);
  },
  [NEW_SET_USER]: (state, user) => {
    state.user = { ...user };
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
