/* eslint no-shadow: ["error", { "allow": ["state"] }] */

import router from '@/router';
import { CLEAR_ALL } from '@/store/actions/user';
import { checkAccessToken, checkRefreshToken } from '@/api/checkAccess';
import CHECK_ACCESS from '../actions/checkAccess';
import { AUTH_ERROR, AUTH_REQUEST, CHECK_SUCCESS } from '../actions/auth';

const actions = {
  /**
   *
   * check user access
   *
   * @param commit
   * @param accessToken
   * @param errConfig
   * @returns {Promise}
   */
  [CHECK_ACCESS]: ({ commit }, { accessToken, errConfig }) => new Promise((resolve, reject) => {
    commit(AUTH_REQUEST);
    checkAccessToken(accessToken)
      .then(() => {
        const token = localStorage.getItem('user-token');
        commit(CHECK_SUCCESS, token);
        resolve(errConfig);
      })
      .catch(() => {
        const refreshToken = { refresh_token: localStorage.getItem('user-token').split(',')[1] };
        checkRefreshToken(refreshToken)
          .then((resp) => {
            const token = `${resp.data.access_token},${resp.data.refresh_token}`;
            let configData = JSON.parse(errConfig.data);
            localStorage.setItem('user-token', token);
            commit(CHECK_SUCCESS, token);
            configData.access_token = resp.data.access_token;
            configData = JSON.stringify(configData);
            errConfig.data = configData;
            // return last request configuration
            resolve(errConfig);
          })
          .catch((err) => {
            commit(AUTH_ERROR, err);
            commit(CLEAR_ALL);
            localStorage.clear();
            router.push('/login');
            reject(err);
          });
      });
  }),
};


export default{
  actions,
};
