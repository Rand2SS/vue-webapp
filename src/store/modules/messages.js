/* eslint no-shadow: ["error", { "allow": ["state"] }] */

import { CHECK_NEW_MESSAGES } from '@/store/actions/messages';
import { getStateOfNewNotifications } from '@/api/messages';

const state = {
  haveNewNotifications: false,
};

const getters = {
};

const actions = {
  [CHECK_NEW_MESSAGES]: ({ commit }) => {
    getStateOfNewNotifications()
      .then((resp) => {
        commit(CHECK_NEW_MESSAGES, resp.data);
      })
      .catch(err => err);
  },
};

const mutations = {
  [CHECK_NEW_MESSAGES]: (state, data) => {
    state.haveNewNotifications = data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
