/* eslint no-shadow: ["error", { "allow": ["state"] }] */

import { login, logout } from '@/api/auth';
import router from '@/router';
import { AUTH_REQUEST, AUTH_SUCCESS, AUTH_ERROR, AUTH_LOGOUT, CHECK_SUCCESS } from '../actions/auth';
import { CLEAR_ALL, SET_USER_ID, SET_USER_ROLE } from '../actions/user';

const state = {
  token: localStorage.getItem('token') || '',
  status: '',
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  getUserToken: state => state.token,
};

const actions = {
  /**
   * @param commit
   * @param user
   * @returns {Promise}
   */
  [AUTH_REQUEST]: ({ commit, dispatch }, user) => new Promise((resolve, reject) => {
    commit(AUTH_REQUEST);
    login(user)
      .then((resp) => {
        const token = `${resp.data.access_token},${resp.data.refresh_token}`;
        const userId = resp.data.userSettings.id;
        const userRole = resp.data.userSettings.role;
        localStorage.setItem('user-token', token);
        commit(AUTH_SUCCESS, token);
        dispatch(SET_USER_ID, userId);
        dispatch(SET_USER_ROLE, userRole);
        resolve(resp);
      })
      .catch((err) => {
        commit(AUTH_ERROR, err);
        localStorage.removeItem('user-token');
        reject(err);
      });
  }),
  /**
   * @param commit
   * @param resetVal
   * @returns {Promise}
   */
  [AUTH_LOGOUT]: ({ commit }, refreshToken) => new Promise((resolve) => {
    commit(AUTH_REQUEST);
    commit(CLEAR_ALL);
    logout(refreshToken)
      .then(() => {
        commit(AUTH_LOGOUT);
        localStorage.clear();
        router.push('/login');
        resolve();
      })
      .catch(() => {
        commit(AUTH_LOGOUT);
        localStorage.clear();
        router.push('/login');
        resolve();
      });
  }),
};

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [AUTH_SUCCESS]: (state, token) => {
    state.status = 'success';
    state.token = token;
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error';
    state.token = '';
  },
  [AUTH_LOGOUT]: (state) => {
    state.status = 'logout';
    state.token = '';
  },
  [CHECK_SUCCESS]: (state, token) => {
    state.status = 'success';
    state.token = token;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
