import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import checkAccess from './modules/checkAccess';
import basket from './modules/basket';
import user from './modules/user';
import messages from './modules/messages';
import newUser from './modules/newUser';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  modules: {
    auth,
    user,
    checkAccess,
    basket,
    messages,
    newUser,
  },
  strict: debug,
});

export default store;
