import store from '@/store/index';
import Vue from 'vue';
import Router from 'vue-router';
import { axiosDefault } from '@/axios/axios';

const Login = () => import('@/views/login.vue');
const Registration = () => import('@/views/Registration.vue');
const Payment = () => import('@/views/Payment.vue');
const PaymentIndex = () => import('@/views/payment/Index.vue');
const PaymentIndexGuid = () => import('@/views/payment/_guid.vue');
const PaymentIndexGuidIndex = () => import('@/views/payment/_guid/Index.vue');
const PaymentIndexGuidSuccess = () => import('@/views/payment/_guid/Success.vue');
const PaymentIndexGuidError = () => import('@/views/payment/_guid/Error.vue');
const PersonalAccountActivation = () => import('@/views/account-activation.vue');
const PersonalAccountActivationIndex = () => import('@/views/account-activation/index.vue');
const PersonalAccountActivationGuid = () => import('@/views/account-activation/_guid.vue');
const PersonalAccountActivationGuidIndex = () => import('@/views/account-activation/_guid/index.vue');
const PersonalAccountActivationGuidSuccess = () => import('@/views/account-activation/_guid/success.vue');
const PersonalAccountActivationGuidError = () => import('@/views/account-activation/_guid/error.vue');
const PrivacyPolicy = () => import('@/views/privacyPolicy.vue');
const Contacts = () => import('@/views/contacts.vue');
const PaymentAndDelivery = () => import('@/views/paymentAndDelivery.vue');
const Warranty = () => import('@/views/warranty.vue');

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/');
};
const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/login');
};
const checkOutAvailable = (to, from, next) => {
  if (!store.state.user.checkOutAvailable) {
    return next('/');
  }
  return next();
};

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        title: 'Автотехника',
      },
      component: () => import('@/views/Home.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/privacy-policy',
      name: 'privacy-policy',
      meta: {
        title: 'Автотехника - политика конфиденциальности',
      },
      component: PrivacyPolicy,
    },
    {
      path: '/contacts',
      name: 'contacts',
      meta: {
        title: 'Автотехника - контакты',
      },
      component: Contacts,
    },
    {
      path: '/payment-and-delivery',
      name: 'payment-and-delivery',
      meta: {
        title: 'Автотехника - оплата и доставка',
      },
      component: PaymentAndDelivery,
    },
    {
      path: '/warranty',
      name: 'warranty',
      meta: {
        title: 'Автотехника - гарантия',
      },
      component: Warranty,
    },
    {
      path: '/login',
      name: 'authorization',
      meta: {
        title: 'Авторизация',
      },
      component: Login,
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/start-settings',
      name: 'startSettings',
      meta: {
        title: 'Авторизация',
      },
      component: () => import('@/views/StartSettings.vue'),
    },
    {
      path: '/registration',
      name: 'registration',
      meta: {
        title: 'Регистрация',
      },
      component: Registration,
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/forgot',
      name: 'forgot',
      meta: {
        title: 'Восстановление пароля',
      },
      component: () => import('@/views/Forgot.vue'),
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/search',
      component: () => import('@/views/SearchIndex.vue'),
      children: [
        {
          path: '',
          name: 'search',
          meta: {
            title: 'Автотехника',
          },
          component: () => import('@/views/Search.vue'),
          props: route => ({ query: route.query.q }),
          beforeEnter: ifAuthenticated,
        },
        {
          path: ':guid',
          name: 'singleProduct',
          meta: {
            title: 'Автотехника',
          },
          component: () => import('@/views/singleProduct.vue'),
          beforeEnter: ifAuthenticated,
        },
      ],
    },
    {
      path: '/catalog',
      name: 'catalog',
      meta: {
        title: 'Автотехника',
      },
      component: () => import('@/views/Catalog/Catalog.vue'),
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: ':guid',
          name: 'catalogItems',
          meta: {
            title: 'Автотехника',
          },
          component: () => import('@/views/Catalog/CatalogItems.vue'),
        },
      ],
    },
    {
      path: '/basket',
      name: 'basket',
      meta: {
        title: 'Корзина',
      },
      component: () => import('@/views/Basket.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/search-by-vin',
      name: 'search-by-vin',
      meta: {
        title: 'Автотехника',
      },
      component: () => import('@/views/SearchByVin.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/check-out',
      name: 'checkOut',
      meta: {
        title: 'Оформление заказа',
      },
      component: () => import('@/views/Checkout.vue'),
      beforeEnter: checkOutAvailable,
    },
    {
      path: '/payment',
      meta: {
        title: 'Оплата заказа',
      },
      component: Payment,
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: '',
          name: 'payment',
          component: PaymentIndex,
          meta: {
            title: 'Автотехника',
          },
        },
        {
          path: ':guid',
          component: PaymentIndexGuid,
          meta: {
            title: 'Автотехника',
          },
          children: [
            {
              path: '',
              name: 'payment-guid',
              component: PaymentIndexGuidIndex,
              meta: {
                title: 'Автотехника',
              },
            },
            {
              path: 'success',
              name: 'payment-guid-success',
              component: PaymentIndexGuidSuccess,
              meta: {
                title: 'Автотехника',
              },
            },
            {
              path: 'error',
              name: 'payment-guid-error',
              component: PaymentIndexGuidError,
              meta: {
                title: 'Автотехника',
              },
            },
          ],
        },
      ],
    },
    {
      path: '/actions',
      name: 'actions',
      meta: {
        title: 'Акции',
      },
      component: () => import('@/views/Actions.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/repair-kit',
      name: 'repairKit',
      meta: {
        title: 'Р./к. суппортов',
      },
      component: () => import('@/views/RepairKit/RepairKit.vue'),
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: ':guid',
          name: 'repairKitItems',
          meta: {
            title: 'Р./к. суппортов',
          },
          component: () => import('@/views/RepairKit/RepairKitItems.vue'),
        },
      ],
    },
    {
      path: '/personal-account',
      name: 'personalAccount',
      meta: {
        title: 'Личный кабинет',
      },
      redirect: { name: 'userInfo' },
      component: () => import('@/views/PersonalAccount.vue'),
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: 'info',
          name: 'userInfo',
          meta: {
            title: 'Личный кабинет - информация',
          },
          component: () => import('@/components/PersonalAccount/info'),
        },
        {
          path: 'integration',
          name: 'integration',
          meta: {
            title: 'Личный кабинет - интеграция',
          },
          component: () => import('@/components/PersonalAccount/integration'),
        },
        {
          path: 'responsible-storage',
          name: 'responsibleStorage',
          meta: {
            title: 'Личный кабинет - реализация',
          },
          component: () => import('@/components/PersonalAccount/responsibleStorage'),
        },
        {
          path: 'users',
          meta: {
            title: 'Личный кабинет - пользователи',
          },
          component: () => import('@/components/PersonalAccount/users'),
          beforeEnter: (to, from, next) => {
            if (store.state.user.userRole === 'user') {
              return next('/');
            }
            return next();
          },
        },
        {
          path: 'reports',
          meta: {
            title: 'Личный кабинет - Заказы',
          },
          component: () => import('@/components/PersonalAccount/reports'),
        },
        {
          path: 'documents',
          meta: {
            title: 'Личный кабинет - документы',
          },
          component: () => import('@/components/PersonalAccount/documents'),
        },
        {
          path: 'messages',
          redirect: { name: 'notifications' },
          meta: {
            title: 'Личный кабинет - сообщения',
          },
          component: () => import('@/components/PersonalAccount/messages/index'),
          beforeEnter: ifAuthenticated,
          children: [
            {
              path: 'notifications',
              name: 'notifications',
              meta: {
                title: 'Оповещения',
              },
              component: () => import('@/components/PersonalAccount/messages/notifications/index'),
            },
            {
              path: 'tickets',
              name: 'tickets',
              meta: {
                title: 'Тикеты',
              },
              component: () => import('@/components/PersonalAccount/messages/tickets/index'),
            },
          ],
        },
        {
          path: 'orders',
          meta: {
            title: 'Заказы',
          },
          component: () => import('@/components/PersonalAccount/orders'),
          beforeEnter: ifAuthenticated,
          children: [
            {
              path: '',
              name: 'ordersList',
              meta: {
                title: 'Заказы',
              },
              component: () => import('@/components/PersonalAccount/orders/ordersTable/orderTable'),
            },
            {
              path: ':orderId',
              meta: {
                title: 'Заказ',
              },
              component: () => import('@/components/PersonalAccount/orders/order/orderInfo'),
            },
          ],
        },
        {
          path: 'refund',
          meta: {
            title: 'Возврат',
          },
          redirect: { name: 'refund' },
          component: () => import('@/components/PersonalAccount/refund'),
          beforeEnter: ifAuthenticated,
          children: [
            {
              path: '',
              name: 'refund',
              meta: {
                title: 'Возврат',
              },
              component: () => import('@/components/PersonalAccount/refund/refundList'),
            },
            {
              path: 'make',
              meta: {
                title: 'Оформление возврата',
              },
              component: () => import('@/components/PersonalAccount/refund/makeRefund/index'),
            },
          ],
        },
      ],
    },
    {
      path: '/account-activation',
      meta: {
        title: 'Активация аккаунта',
      },
      component: PersonalAccountActivation,
      children: [
        {
          path: '',
          component: PersonalAccountActivationIndex,
        },
        {
          path: ':guid',
          component: PersonalAccountActivationGuid,
          children: [
            {
              path: '',
              name: 'account-activation-guid-index',
              meta: {
                title: 'Активация аккаунта',
              },
              component: PersonalAccountActivationGuidIndex,
            },
            {
              path: 'success',
              name: 'account-activation-guid-success',
              meta: {
                title: 'Активация аккаунта - успешно',
              },
              component: PersonalAccountActivationGuidSuccess,
            },
            {
              path: 'error',
              name: 'account-activation-guid-error',
              meta: {
                title: 'Активация аккаунта - ошибка',
              },
              component: PersonalAccountActivationGuidError,
            },
          ],
        },
      ],
    },
    {
      path: '/pass/new/:guid',
      name: 'newPassword',
      meta: {
        title: 'Новый пароль',
      },
      component: () => import('@/views/NewPassword.vue'),
      beforeEnter: (to, from, next) => {
        axiosDefault.post(`/pass/new/${to.params.guid}`, {})
          .then((resp) => {
            if (resp.data) {
              next();
            } else {
              next('/login');
            }
          })
          .catch(err => err);
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
