import { axiosDefault } from '@/axios/axios';

export function sendRegistrationParams(params) {
  return axiosDefault.post('/reg', params);
}

export function checkNameAvailability(name) {
  return axiosDefault.post('/aval_name', name);
}

export function checkEmailAvailability(email) {
  return axiosDefault.post('/aval_email', email);
}

export function getRegionList() {
  return axiosDefault.post('/reg/region-list ', {});
}
