import { axiosDefault } from '@/axios/axios';

export const regUser = data => axiosDefault({
  url: '/reg/new',
  method: 'post',
  data,
});

export const activateUser = guid => axiosDefault({
  url: '/reg/activation',
  method: 'post',
  data: {
    guid,
  },
});

const registrationNew = {
  regUser,
  activateUser,
};

export default registrationNew;
