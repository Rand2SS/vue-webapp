import axios from '@/axios/axios';

export function addToBasket(params) {
  return axios.post('/cart/add', params);
}
export function deleteFromBasket(params) {
  return axios.post('/cart/delete', params);
}

export function clearBasket() {
  return axios.post('/cart/clear', {});
}
