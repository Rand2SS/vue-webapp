import { axiosDefault } from '@/axios/axios';

export function login(user) {
  return axiosDefault.post('/login', user);
}

export function logout(refreshToken) {
  return axiosDefault.post('/logout', {
    refresh_token: refreshToken,
  });
}

/**
 * @param email - current email
 * @param domain - current domain
 * @returns {AxiosPromise<any>}
 */

export function forgotPassword(email, domain) {
  return axiosDefault.post('/pass/change', {
    email,
    domain,
  });
}

/**
 * @param guid - change pass guid
 * @param pass - current pass
 * @returns {AxiosPromise<any>}
 */
export function newPasswordSet(guid, pass) {
  return axiosDefault.post(`/pass/set/${guid}`, {
    guid,
    pass,
  });
}
