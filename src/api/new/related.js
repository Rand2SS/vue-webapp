import axios from '@/axios/axios';

function getList() {
  return axios.post('/related/get_list', {});
}

const relatedApi = {
  getList,
};

export default relatedApi;
