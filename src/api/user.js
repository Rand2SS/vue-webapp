import axios from '@/axios/axios';

function getUser() {
  return axios.post('/user/get_info', {});
}

const userApi = {
  getUser,
};

export default userApi;
