import axios from '@/axios/axios';

function get() {
  return axios({
    url: '/user/all-addresses',
    method: 'post',
    data: {},
  });
}

const addressesApi = {
  get,
};

export default addressesApi;
