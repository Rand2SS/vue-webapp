import axios from '@/axios/axios';

// TODO: удалить коммент, если успех
// export const searchRequest = sku => axios.post('/search', sku);
export const searchRequest = sku => axios.post('/search', sku);

/**
 * @param address
 * @param params
 * @returns {AxiosPromise<any>}
 */
export const searchResult = params => axios.post('/product-search', {}, { params });
