import { axiosDefault } from '@/axios/axios';

export function checkAccessToken(token) {
  return axiosDefault.post('/check_access_token', token);
}

export function checkRefreshToken(token) {
  return axiosDefault.post('/access_token', token);
}
