import axios from '@/axios/axios';

export function getNotificationApi() {
  return axios.post('/messages/notifications', {});
}

export function setNotificationAsReadApi(guid) {
  return axios.post(`/messages/notifications/${guid}`, {});
}

export function getStateOfNewNotifications() {
  return axios.post('/messages/notifications/actual', {});
}
